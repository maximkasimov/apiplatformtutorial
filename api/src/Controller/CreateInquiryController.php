<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CreateInquiryController extends AbstractController
{
    /**
     * @Route("/api/create_inquiry", name="create_inquiry", methods={"POST"})
     */
    public function index(Request $request): Response
    {
        $params = array();
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $params = json_decode($request->getContent(), true);
        }

        return $this->json([
            'status' => 'success',
            'message' => 'No errors',
        ]);
    }
}

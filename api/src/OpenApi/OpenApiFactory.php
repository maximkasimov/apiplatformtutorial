<?php
declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

final class OpenApiFactory implements OpenApiFactoryInterface
{
    public function __construct(private OpenApiFactoryInterface $decorated){}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Inquiry'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'firstname' => [
                    'type' => 'string',
                    'example' => 'Maxim',
                ],
                'city' => [
                    'type' => 'string',
                    'example' => 'Kyiv',
                ],
                'phone' => [
                    'type' => 'string',
                    'example' => '+380123456789',
                ],
                'email' => [
                    'type' => 'string',
                    'example' => 'maxim@example.com',
                ],
            ],
        ]);

        $schemas['InquiryResult'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'status' => [
                    'type' => 'string',
                    'readOnly' => true,
                    'example' => 'success'
                ],
                'message' => [
                    'type' => 'string',
                    'readOnly' => true,
                    'example' => 'No error'
                ],
            ],
        ]);

        $pathItem = new Model\PathItem(
            ref: 'Inquiry',
            post: new Model\Operation(
                operationId: 'postInquiryItem',
                tags: ['CreateInquiry'],
                responses: [
                    '200' => [
                        'description' => 'Create new inquiry',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/InquiryResult',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Create new inquiry to order services.',
                requestBody: new Model\RequestBody(
                    description: 'Create new Inquiry',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Inquiry',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/api/create_inquiry', $pathItem);

        return $openApi;
    }
}
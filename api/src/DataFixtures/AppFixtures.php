<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Location;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->makeLocations($manager);
        $manager->flush();
    }

    /**
     * @return Location
     */
    private function makeLocation(
        ObjectManager $manager, 
        array $params
        ): Location
    {
        $location = new Location();
        $location
            ->setNameUa($params["nameUa"])
            ->setNameRu($params["nameRu"])
            ->setUrlName($params["urlName"])
            ;
        $manager->persist($location);
        return $location;
    }

    private function makeLocations(ObjectManager $manager)
    {
        foreach (array(
            array(
                'nameUa' => "Київ",
                'nameRu' => "Киев",
                'urlName' => "kyiv",
            ),
            array(
                'nameUa' => "Біла Церква",
                'nameRu' => "Белая Церковь",
                'urlName' => "bilacerkva",
            ),
            array(
                'nameUa' => "Вінниця",
                'nameRu' => "Винница",
                'urlName' => "vinnytsia",
            ),
            array(
                'nameUa' => "Волочиськ",
                'nameRu' => "Волочиск",
                'urlName' => "volochysk",
            ),
            array(
                'nameUa' => "Дніпро",
                'nameRu' => "Днепр",
                'urlName' => "dnipro",
            ),
            array(
                'nameUa' => "Добротвір",
                'nameRu' => "Добротвор",
                'urlName' => "dobrotvir",
            ),
            array(
                'nameUa' => "Івано-Франківськ",
                'nameRu' => "Ивано-Франковск",
                'urlName' => "ivanofrankivsk",
            ),
            array(
                'nameUa' => "Житомир",
                'nameRu' => "Житомир",
                'urlName' => "zhytomyr",
            ),
            array(
                'nameUa' => "Запоріжжя",
                'nameRu' => "Запорожье",
                'urlName' => "zaporizhia",
            ),
            array(
                'nameUa' => "Краматорськ",
                'nameRu' => "Краматорск",
                'urlName' => "kramatorsk",
            ),
            array(
                'nameUa' => "Кременець",
                'nameRu' => "Кременец",
                'urlName' => "kremenets",
            ),
            array(
                'nameUa' => "Кривий Ріг",
                'nameRu' => "Кривой Рог",
                'urlName' => "kryvyirih",
            ),
            array(
                'nameUa' => "Кропивницький",
                'nameRu' => "Кропивницкий",
                'urlName' => "kropyvnytskyi",
            ),
            array(
                'nameUa' => "Луцьк",
                'nameRu' => "Луцк",
                'urlName' => "lutsk",
            ),
            array(
                'nameUa' => "Львів",
                'nameRu' => "Львов",
                'urlName' => "lviv",
            ),
            array(
                'nameUa' => "Мелітополь",
                'nameRu' => "Мелитополь",
                'urlName' => "melitopol",
            ),
            array(
                'nameUa' => "Обухів",
                'nameRu' => "Обухов",
                'urlName' => "obukhiv",
            ),
            array(
                'nameUa' => "Полтава",
                'nameRu' => "Полтава",
                'urlName' => "poltava",
            ),
            array(
                'nameUa' => "Путивль",
                'nameRu' => "Путивль",
                'urlName' => "putyvl",
            ),
            array(
                'nameUa' => "Рівне",
                'nameRu' => "Ровно",
                'urlName' => "rivne",
            ),
            array(
                'nameUa' => "Солоницівка",
                'nameRu' => "Солоницевка",
                'urlName' => "solonytsivka",
            ),
            array(
                'nameUa' => "Суми",
                'nameRu' => "Сумы",
                'urlName' => "sumy",
            ),
            array(
                'nameUa' => "Теребовля",
                'nameRu' => "Теребовля",
                'urlName' => "terebovlia",
            ),
            array(
                'nameUa' => "Тернопіль",
                'nameRu' => "Тернополь",
                'urlName' => "ternopil",
            ),
            array(
                'nameUa' => "Трускавець",
                'nameRu' => "Трускавец",
                'urlName' => "truskavets",
            ),
            array(
                'nameUa' => "Українка",
                'nameRu' => "Украинка",
                'urlName' => "ukrainka",
            ),
            array(
                'nameUa' => "Фастів",
                'nameRu' => "Фастов",
                'urlName' => "fastiv",
            ),
            array(
                'nameUa' => "Харків",
                'nameRu' => "Харьков",
                'urlName' => "kharkiv",
            ),
            array(
                'nameUa' => "Херсон",
                'nameRu' => "Херсон",
                'urlName' => "kherson",
            ),
            array(
                'nameUa' => "Хмельницький",
                'nameRu' => "Хмельницкий",
                'urlName' => "khmelnytskyi",
            ),
            array(
                'nameUa' => "Черкаси",
                'nameRu' => "Черкассы",
                'urlName' => "cherkasy",
            ),
            array(
                'nameUa' => "Чернівці",
                'nameRu' => "Черновцы",
                'urlName' => "chernivtsi",
            ),
            array(
                'nameUa' => "Чортків",
                'nameRu' => "Чортков",
                'urlName' => "chortkiv",
            ),
            array(
                'nameUa' => "Одеса",
                'nameRu' => "Одесса",
                'urlName' => "odesa",
            ),
        ) as &$params) {
            $this->makeLocation($manager, $params);
        }
    }
}
